import Vue from 'vue';
import Vuex from 'vuex';
import api from '@/api';
// eslint-disable-next-line import/extensions
import getMockData from '../mocks/getPayments.js';

Vue.use(Vuex);

const getDefaultState = () => {
  return {
    data: [],
    isLoading: false,
    isCached: false,
  };
};

export default new Vuex.Store({

  state: () => getDefaultState(),

  mutations: {
    setState(state, value) {
      Object.entries(value).forEach(([key, data]) => {
        if (!Array.isArray(state[key]) && state[key] && typeof state[key] === 'object') {
          state[key] = {
            ...state[key],
            ...data,
          };
        } else {
          state[key] = data;
        }
      });
    },
    resetState(state) {
      Object.assign(state, getDefaultState());
    },
  },

  actions: {
    resetCacheState({ commit }) {
      commit('resetState');
    },
    async load({ commit }, params = {}) {
      commit('setState', { isLoading: true });

      try {
        const { data } = await api.getPayments(params);

        if (Array.isArray(data)) {
          commit('setState', { data });
        }
        commit('setState', { isCached: true });
      } catch (e) {
        // eslint-disable-next-line no-alert
        alert(e?.message);

        const { data } = await getMockData();

        if (Array.isArray(data)) {
          commit('setState', { data });
        }
        commit('setState', { isCached: true });
      } finally {
        commit('setState', { isLoading: false });
      }
    },
  },
});
